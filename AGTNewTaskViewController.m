//
//  AGTNewTaskViewController.m
//  ToDoList
//
//  Created by Maria Jose Olivares Sempere on 13/4/15.
//  Copyright (c) 2015 Agbo Training. All rights reserved.
//

#import "AGTNewTaskViewController.h"
#import "AGTTaskModel.h"

@interface AGTNewTaskViewController ()

@end

@implementation AGTNewTaskViewController

- (id) initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        _myNewTask = [[AGTTaskModel alloc] init];
        self.title = @"New Task";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    self.myNewTask.task = self.textFieldView.text;
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


@end
