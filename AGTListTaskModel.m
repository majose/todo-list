//
//  AGTListTaskModel.m
//  ToDoList
//
//  Created by Maria Jose Olivares Sempere on 13/4/15.
//  Copyright (c) 2015 Agbo Training. All rights reserved.
//

#import "AGTListTaskModel.h"

#import "AGTTaskModel.h"

@interface AGTListTaskModel ()

@property (strong, nonatomic) NSMutableArray *tasks;

@end

@implementation AGTListTaskModel

-(id) init{
    
    if (self = [super init]){
        
        _tasks = [[NSMutableArray alloc] init];
        
        AGTTaskModel *task1 = [[AGTTaskModel alloc] init];
        task1.task = @"Práctica iOS Intermedio";
        task1.done = NO;
        
        [self.tasks addObject:task1];
        
        AGTTaskModel *task2 = [[AGTTaskModel alloc] init];
        task2.task = @"Online Swift";
        task2.done = NO;
        
        [self.tasks addObject:task2];
        
    }
    
    return self;
}




-(void) addTask: (AGTTaskModel *) aTask{
    
    [self.tasks addObject:aTask];
}

-(void) removeTaskAtIndex:(NSUInteger) anIndex{
    
    [self.tasks removeObjectAtIndex:anIndex];
}

-(AGTTaskModel *) taskAtIndex: (NSUInteger) anIndex{
   
    return [self.tasks objectAtIndex:anIndex];
    
}


-(NSUInteger) countTask{
    
    return [self.tasks count];
}

@end
