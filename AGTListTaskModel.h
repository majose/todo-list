//
//  AGTListTaskModel.h
//  ToDoList
//
//  Created by Maria Jose Olivares Sempere on 13/4/15.
//  Copyright (c) 2015 Agbo Training. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AGTTaskModel;

@interface AGTListTaskModel : NSObject

-(void) addTask: (AGTTaskModel *) aTask;
-(void) removeTaskAtIndex:(NSUInteger) anIndex;
-(AGTTaskModel *) taskAtIndex: (NSUInteger) anIndex;
-(NSUInteger) countTask;

@end
