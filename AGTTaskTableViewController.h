//
//  AGTTaskTableViewController.h
//  ToDoList
//
//  Created by Maria Jose Olivares Sempere on 13/4/15.
//  Copyright (c) 2015 Agbo Training. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AGTListTaskModel;

@interface AGTTaskTableViewController : UITableViewController 

@property (strong,nonatomic)AGTListTaskModel *model;


@end
