//
//  AGTNewTaskViewController.h
//  ToDoList
//
//  Created by Maria Jose Olivares Sempere on 13/4/15.
//  Copyright (c) 2015 Agbo Training. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AGTTaskModel;

@interface AGTNewTaskViewController : UIViewController

@property (strong, nonatomic) AGTTaskModel *myNewTask;

@property (weak, nonatomic) IBOutlet UITextField *textFieldView;


@end
