//
//  AGTTaskModel.m
//  ToDoList
//
//  Created by Maria Jose Olivares Sempere on 13/4/15.
//  Copyright (c) 2015 Agbo Training. All rights reserved.
//

#import "AGTTaskModel.h"

@implementation AGTTaskModel

-(id) init{
    
    if (self = [super init]){
        
        _task = nil;
        _done = NO;
    }
    
    return self;
}

@end
