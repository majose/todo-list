//
//  AGTTaskModel.h
//  ToDoList
//
//  Created by Maria Jose Olivares Sempere on 13/4/15.
//  Copyright (c) 2015 Agbo Training. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AGTTaskModel : NSObject

@property (copy, nonatomic) NSString * task;
@property   (nonatomic) BOOL done;

@end
